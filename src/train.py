import random
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.models import load_model
from tensorflow.keras.models import Sequential
import numpy as np
import pickle
import json
import nltk
from nltk.stem import WordNetLemmatizer

# WordNetLemmatizer method called to perform lemmatization
# Allows us to recognise variations of words as the same - makes processing them more efficient
lemmatizer = WordNetLemmatizer()

nltk.download("punkt")
nltk.download("wordnet")

words = []
tags = []
tokenized_words = []
ignore_words = ["?", "!"]

# Read in JSON data from intents file and convert to a python dictionary
with open("intents.json", "r") as file:
    data=file.read()
    if data:
        intents = json.loads(data)
    else:
        print("No data")

# Iterate through data in intents file
for intent in intents["intents"]:

    # Iterate through patterns in intents - ie. the list of messages the chatbot is likely to recieve
    for patterns in intent["patterns"]:

        # Tokenize words in those sentences - ie. Split sentence up into the individual words it's made up of
        w = nltk.word_tokenize(patterns)

        # Add these words to the words list
        # Use extend to iterate over all the words and extend them to the list instead of appending individually
        words.extend(w)

        # Add tokenized words to tokenized_words along with the tag they correspond to in the intents file
        tokenized_words.append((w, intent["tag"]))

        # Add each tag to a list called tags
        if intent["tag"] not in tags:
            tags.append(intent["tag"])

# Lemmatize each word in the words list (words from sentences that the chatbot can understand)
# This process is to group together inflected or variant forms of the same word - make processing them easier
words = [lemmatizer.lemmatize(w.lower()) for w in words if w not in ignore_words]

# Create a sorted list of the set of lemmatized words the chatbot can undertsand
# Each set of words corresponds to a particular tag in the intents file
words = sorted(list(set(words)))

# Created a sorted list of the set of tags that correspond to the lemmatized words the chatbot can understand
tags = sorted(list(set(tags)))

print(len(tokenized_words), "tokenized_words")  # Print out tokenized words along with their tag
print(len(tags), "tags", tags)  # Print out list of tags the chatbot understands
print(len(words), "Lemmatized words", words)  # Print out lemmatized words list

# Dump lemmatized words and list of tags into pickle files for storage
pickle.dump(words, open("words.pkl","wb"))
pickle.dump(tags, open("tags.pkl","wb"))

# Create training data
# Need to change the document of words into tensors of numbers so tensorlfow model can recognise them
training = []

# Empty array for the output
output_empty = [0] * len(tags)
for token in tokenized_words:

    # Initialize bag of words
    bag = []

    # List of tokenized words
    pattern_words = token[0]

    # Lemmatize each word, create a base word to represent all related words
    pattern_words = [lemmatizer.lemmatize(word.lower()) for word in pattern_words]

    # If a word match is found in word pattern lemmatzed words list, create bag array with 1, else create array with 0
    for w in words:
        bag.append(1) if w in pattern_words else bag.append(0)

    # For each tag, the output is 0 and for the current tag, it is 1.
    output_row = list(output_empty)
    output_row[tags.index(token[1])] = 1

    # Append bag of words and output to training array
    training.append([bag, output_row])

# Shuffle features and create a numpy array for training data
random.shuffle(training)
training = np.array(training)

# Create train and test lists
train_x = list(training[:,0])
train_y = list(training[:,1])
print("Training data created")

# Create 3-layer output model
model = Sequential()
model.add(Dense(128, input_shape=(len(train_x[0]),), activation="relu"))
model.add(Dropout(0.5))
model.add(Dense(64, activation="relu"))
model.add(Dropout(0.5))
model.add(Dense(len(train_y[0]), activation="softmax"))
model.summary()

# Compile the chatbot model
sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(loss="categorical_crossentropy", optimizer=sgd, metrics=["accuracy"])

# Fit and save the model
hist = model.fit(np.array(train_x), np.array(train_y), epochs=200, batch_size=5, verbose=1)
model.save("chatbot_model.h5", hist)
print("model created")
