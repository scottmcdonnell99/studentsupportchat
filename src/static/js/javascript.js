
 const messengerForm = get(".messenger-inputarea");
 const messengerInput = get(".messenger-input");
 const messengerChat = get(".messenger-chat");

 // Icon made by DCU
 const CHATBOT_IMAGE = "https://www.dcu.ie/sites/default/files/marketing/images/dcu_logo_stacked_red.png";
 // Icon made by Becris from @flaticon)
 const STUDENT_IMAGE =  "https://cdn-icons-png.flaticon.com/512/847/847969.png";
 const CHATBOT_NAME = "DCU";
 const STUDENT_NAME = "Student";


    messengerForm.addEventListener("submit", event => {
      event.preventDefault();

      const message = messengerInput.value;
      if (!message) return;

      NewMessage(STUDENT_NAME, STUDENT_IMAGE, "right", message);
      messengerInput.value = "";
      ChatbotMessage(message);
    });

    // appends new message
    function NewMessage(name, icon, side, message) {
      const html = `
<div class="message ${side}-message">
  <div class="message-img" style="background-image: url(${icon})"></div>

  <div class="message-bubble">
    <div class="message-info">
      <div class="message-info-name">${name}</div>
      <div class="message-info-time">${formatDate(new Date())}</div>
    </div>

    <div class="message-text">${message}</div>
  </div>
</div>
`;

      messengerChat.insertAdjacentHTML("beforeend", html);
      messengerChat.scrollTop += 500;
    }
    // appends chatbot response
    function ChatbotMessage(rawText)
    {
      $.get("/get", { msg: rawText }).done(function (data) {
        console.log(rawText);
        console.log(data);
        const message = data;
        NewMessage(CHATBOT_NAME, CHATBOT_IMAGE, "left", message);

      });
    }
    //
    function get(selector, root = document)
    {
      return root.querySelector(selector);
    }

    function formatDate(date)
    {
      const h = "0" + date.getHours();
      const m = "0" + date.getMinutes();

      return `${h.slice(-2)}:${m.slice(-2)}`;
    }