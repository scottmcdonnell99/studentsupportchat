import random
import numpy as np
import pickle
import json
from flask import Flask, render_template, request
import nltk
from tensorflow.keras.models import load_model
from nltk.stem import WordNetLemmatizer
from nltk.metrics.distance import jaccard_distance
from nltk.util import ngrams
import json
import random

nltk.download('words')
from nltk.corpus import words

correct_words = words.words()

# WordNetLemmatizer method called to perform lemmatization
# Allows us to recognise variations of words as the same - makes processing them more efficient
lemmatizer = WordNetLemmatizer()

model = load_model('chatbot_model.h5')  # Load chatbot model
intents = json.loads(open('intents.json').read())  # Load JSON data
words = pickle.load(open('words.pkl','rb'))  # Load lemmatized words
classes = pickle.load(open('tags.pkl','rb'))  # Load list of tags

app = Flask(__name__)
app.static_folder = 'static'

# Home route
@app.route("/")
def home():
    return render_template("index.html")

# get request
@app.route("/get")
def get_bot_response():
    textMsg = request.args.get('msg')
    return chatbot_response(textMsg)  # Pass message into chatbot_response to get a response

def tidy_sentence(sentence):
    sentence_words = nltk.word_tokenize(sentence)  # Tokenize sentence
    sentence_words = [lemmatizer.lemmatize(word.lower()) for word in sentence_words]  # Lemmatize sentence

    return sentence_words

def bag_of_words(sentence, words, show_details=True):
    sentence_words = tidy_sentence(sentence)  # Pass sentence into tidy_sentence to tokenize and lemmatize it

    bag = [0] * len(words)
    # Return array - 0 or 1 for each word that is in the sentence
    for s in sentence_words:
        for i, w in enumerate(words):
            if w == s:
                bag[i] = 1
                if show_details:
                    print("found in bag of words: %s" %w)

    return np.array(bag)

context = {}

def predict_class(sentence, model):
    # Filter out response predictions below ERROR_THRESHOLD
    p = bag_of_words(sentence, words, show_details=False)
    res = model.predict(np.array([p]))[0]
    ERROR_THRESHOLD = 0.25
    results = [[i,r] for i,r in enumerate(res) if r>ERROR_THRESHOLD]

    # Sort predictions by strength of probability
    results.sort(key=lambda x: x[1], reverse=True)
    return_list = []
    for r in results:
        return_list.append({"intent": classes[r[0]], "probability": str(r[1])})
    return return_list

def getResponse(ints, intents_json, userID='123', show_details=False):
    tag = ints[0]['intent']
    list_of_intents = intents_json['intents']
    # Get a chatbot response
    for i in list_of_intents:
        if(i['tag'] == tag):
            # Check for a context set in tag
            if "context_set" in i:
                if show_details: return 'context: ', i['context_set']
                context[userID] = i['context_set']

            # If there's a context set, look for a matching context filter and draw a response from that tag
            if not 'context_filter' in i or (userID in context and 'context_filter' in i and i['context_filter'] == context[userID]):
                if show_details: return ('tag:', i['tag'])
                result = random.choice(i['responses'])
                return result

            else:
                result = random.choice(i['responses'])
                return result

def chatbot_response(textMsg):
    ints = predict_class(textMsg, model)
    res = getResponse(ints, intents)
    return res

if __name__ == "__main__":
    app.run()